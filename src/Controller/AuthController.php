<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;


class AuthController extends AbstractController
{
    private $encoderFactory;

    public function __construct(EncoderFactoryInterface $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * @Route("/auth", name="auth")
     */
    public function index(): Response
    {
        return $this->render('auth/index.html.twig', [
            'controller_name' => 'AuthController',
        ]);
    }

    public function createUser(Request $request)
    {

        $parameters = json_decode($request->getContent(), true);

        if (isset($parameters['email']) && isset($parameters['password']) && isset($parameters['username'])) {
            $entityManager = $this->getDoctrine()->getManager();
            $testUser = new User();
            $testUser->setEmail($parameters['email']);
            $testUser->setUsername($parameters['username']);
            $encoder = $this->encoderFactory->getEncoder($testUser);
            $pwd = $encoder->encodePassword($parameters['password'], 'nosaltatthispoint');
            $testUser->setPassword($pwd);
            $testUser->setRoles(['ADMIN', 'ROLE_USER']);
            $entityManager->persist($testUser);
            try {
                $entityManager->flush();
            } catch (\Exception $e) {
                return new Response(json_encode(['status' => 'error', 'message' => 'This email is in use']), '400');
            }
            return new Response(json_encode(['status' => 'created']), '201');
        }
        return new Response(json_encode(['status' => 'error', 'message' => 'Showing this error']), '400');
    }
}
