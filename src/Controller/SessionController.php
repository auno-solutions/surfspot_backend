<?php

namespace App\Controller;

use App\Entity\Session;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SessionController extends AbstractController
{
    /*
     * @param EntityManagerInterface $entityManager plain text
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function fetchSessionCount($id): Response
    {
        try {
            $sessionRepo = $this->entityManager->getRepository(Session::class);
            $qb = $sessionRepo->createQueryBuilder('u');
            $res = $qb->select('count(u.id)')
                ->where('u.user = ' . $id)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (EntityNotFoundException $e) {
            return new Response(json_encode(['status' => 'error', 'message' => 'Invalid']), '400');
        }
        return new Response(json_encode(['status' => 'succes', 'data' => ['count' => $res]]), '200');
    }

    public function fetchSessionGraph($id): Response
    {
        //should be in the repo
        $sql = "SELECT left(MONTHNAME(seesion_start),3) as month,count(id) as counter FROM session WHERE user_id = " . $id . " GROUP BY MONTH(seesion_start)";

        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Session::class, 'u');
        $rsm->addScalarResult('month', 'month');
        $rsm->addScalarResult('counter', 'counter');

        $query = $this->entityManager->createNativeQuery($sql, $rsm);
        $queryResult = $query->getResult();

        $dataMonths = $this->getMonthsArray();

        $datasetData = [];
        foreach ($dataMonths as $month) {
            $match = false;
            $tempMatch = [];

            foreach ($queryResult as $result) {
                if ($result['month'] === $month) {
                    $match = true;
                    $tempMatch = $result;
                    break;
                }
            }

            if ($match) {
                array_push($datasetData, (integer)$tempMatch['counter']);
            } else {
                array_push($datasetData, 0);
            }

        }


        $postSet = [];
        array_push($postSet, [
                "data" => $datasetData,
                "color" => "rgba(134, 65, 244,1)",
                "strokeWidth" => 2
            ]
        );

        if(count($queryResult) == 0){
            $postSet = [];
        }


        $data = [
            "labels" => $dataMonths,
            "datasets" => $postSet,
            "legend" => "Sessions"
        ];


        return new Response(json_encode(['status' => 'succes', 'data' => $data]), '200');
    }

    public function fetchSportGraph($id): Response
    {
        //should be in the repo
        $sql = "SELECT sport.sport_name, left(MONTHNAME(session.seesion_start),3) as month,count(session.id) as counter FROM session left JOIN sport on session.sport_id = sport.id WHERE user_id = " . $id . " GROUP BY MONTH(seesion_start), sport_id";

        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Session::class, 'u');
        $rsm->addScalarResult('month', 'month');
        $rsm->addScalarResult('sport_name', 'sport_name');
        $rsm->addScalarResult('counter', 'counter');

        $query = $this->entityManager->createNativeQuery($sql, $rsm);
        $queryResult = $query->getResult();
        $dataMonths = $this->getMonthsArray();

        $datasetData = [];
        $sportrSet = [];
        foreach ($queryResult as $item) {
            if (!isset($sportrSet[$item['sport_name']])) {
                $sportrSet[$item['sport_name']] = [];
            }
            array_push($sportrSet[$item['sport_name']], $item);
        }

        foreach ($sportrSet as $sportData) {
            foreach ($dataMonths as $month) {
                foreach ($sportData as $dataItem) {
                    $match = false;
                    $tempMatch = [];
                    if (!isset($datasetData[$dataItem['sport_name']])) {
                        $datasetData[$dataItem['sport_name']] = [];
                    }
                    if ($dataItem['month'] === $month) {
                        $match = true;
                        $tempMatch = $dataItem;
                        break;
                    }
                }

                if ($match) {
                    array_push($datasetData[$dataItem['sport_name']], $tempMatch['counter']);
                } else {
                    array_push($datasetData[$dataItem['sport_name']], 0);
                }

            }
        }

        $postSet = [];

        foreach ($datasetData as $sport => $set) {
            array_push($postSet, [
                    'data' => $set,
                    'color' => "134, 65, 244,",
                    'strokeWidth' => 2,
                    'sport' => $sport
                ]
            );
        }

        $data = [
            'labels' => $dataMonths,
            'datasets' => $postSet,
            'legend' => array_keys($datasetData)
        ];

        return new Response(json_encode(['status' => 'succes', 'data' => $data]), '200');
    }

    private function getMonthsArray()
    {
        $monthsHistory = 4;
        for ($i = 0; $i < $monthsHistory; $i++) {
            $months[] = date("M", strtotime(date('Y-m-01') . " -$i months"));
        }

        return array_reverse($months);
    }

}
