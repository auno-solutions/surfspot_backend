<?php

namespace App\Controller;

use App\Entity\FavoriteUserSpots;
use App\Entity\Gear;
use App\Entity\Spots;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SpotController extends BaseController
{
    public function fetchUserSavedSpots($id)
    {
        $spotRepo = $this->entityManager->getRepository(Spots::class);
        $userFavoSpotsRepo = $this->entityManager->getRepository(FavoriteUserSpots::class);
        $userFavoSpots = $userFavoSpotsRepo->findBy(['userId' => $id]);

        $resultSet = $spotRepo->findBy(['user' => $id]);



        $dupset = [];

        foreach ($userFavoSpots as $favoriteUserSpots){
            array_push($resultSet,$favoriteUserSpots->getSpotId());
        }

        $dataSet = [];

        foreach ($resultSet as $item) {
            if(!array_key_exists($item->getId(),$dupset)){
                $data = [
                    'id' => $item->getId(),
                    'name' => $item->getName(),
                    'description' => $item->getDescription(),
                    'latitude' => $item->getLattitude(),
                    'longitude' => $item->getLongitude(),
                    'likes' => $item->getLikes(),
                ];
                array_push($dataSet, $data);
            }
        }

        return new Response(json_encode(['status' => 'succes', 'set' => $dataSet]), '200');
    }

    public function fetchNearestSpotsFromCoord(Request $request)
    {
        $parameters = json_decode($request->getContent(), true);
        $coord = $parameters['coord'];

        $spotRepo = $this->entityManager->getRepository(Spots::class);
        $result = $spotRepo->findNearestSpotsByLocaiton($coord['latitude'], $coord['longitude'], $coord['spotRange']);

        return new Response(json_encode(['status' => 'succes', 'data' => $result]), '200');
    }

    public function fetchSpotDetail(Request $request)
    {
        $parameters = json_decode($request->getContent(), true);


        $spot = $parameters['spot'];

        $spotRepo = $this->entityManager->getRepository(Spots::class);
        $result = $spotRepo->findSpotDetails($spot);

        return new Response(json_encode(['status' => 'succes', 'data' => $result]), '200');
    }

    public function fetchSpotLeaderboard(Request $request)
    {
        $parameters = json_decode($request->getContent(), true);
        $id = $parameters['id'];

        $spotRepo = $this->entityManager->getRepository(Spots::class);
        $result = $spotRepo->generateLeaderBoard($id);

        return new Response(json_encode(['status' => 'succes', 'data' => $result]), '200');
    }

    public function index(): Response
    {
        return $this->render('spot/index.html.twig', [
            'controller_name' => 'SpotController',
        ]);
    }

    public function updateUserFavorites(Request $request)
    {
        try {
            $gearRepo = $this->entityManager->getRepository(Gear::class);
            $parameters = json_decode($request->getContent(), true);
            $gearRepo->updateUserFavoritesByGearId($parameters);

        }catch (Exception $e){
            return new Response(json_encode(['status' => 'error', 'message' => 'Invalid']), '400');
        }

        return new Response(json_encode(['status' => 'succes', 'data' => 'updated']), '200');
    }

}
