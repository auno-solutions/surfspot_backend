<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FavoriteUserSpotsRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=FavoriteUserSpotsRepository::class)
 * @ApiFilter(SearchFilter::class, properties={"userId": "exact","spotId","exact"})
 */

class FavoriteUserSpots
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="favoriteUserSpots")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userId;

    /**
     * @ORM\ManyToOne(targetEntity=Spots::class, inversedBy="favoriteUserSpots")
     * @ORM\JoinColumn(nullable=false)
     */
    private $spotId;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?User $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getSpotId(): ?Spots
    {
        return $this->spotId;
    }

    public function setSpotId(?Spots $spotId): self
    {
        $this->spotId = $spotId;

        return $this;
    }
}
