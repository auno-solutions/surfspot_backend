<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Repository\GearRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ORM\Entity(repositoryClass=GearRepository::class)
 * @ApiResource(attributes={
 *     "normalization_context"={"groups"={"gear:read"}}
 * })
 * @ApiFilter(SearchFilter::class, properties={"user": "exact"})
 */

class Gear
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("gear:read")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="gears")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("gear:read")
     */
    private $user;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("gear:read")
     */
    private $value;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("gear:read")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("gear:read")
     */
    private $type_value;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("gear:read")
     */
    private $brand;

    /**
     * @ORM\ManyToOne(targetEntity=Sport::class, inversedBy="gears",fetch="LAZY")
     * @Groups("gear:read")
     */
    private $sport;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups("gear:read")
     */
    private $user_default;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }
    
    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTypeValue(): ?string
    {
        return $this->type_value;
    }

    public function setTypeValue(?string $type_value): self
    {
        $this->type_value = $type_value;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(?string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getSport(): ?Sport
    {
        return $this->sport;
    }

    public function setSport(?sport $sport): self
    {
        $this->sport = $sport;

        return $this;
    }

    public function getUserDefault(): ?bool
    {
        return $this->user_default;
    }

    public function setUserDefault(?bool $user_default): self
    {
        $this->user_default = $user_default;

        return $this;
    }
}
