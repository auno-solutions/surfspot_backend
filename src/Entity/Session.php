<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\SessionController;
use App\Repository\SessionRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ApiResource(attributes={
 *     "normalization_context"={"groups"={"read:userlist"}}
 * })
 * @ORM\Entity(repositoryClass=SessionRepository::class)
 * @ApiFilter(SearchFilter::class, properties={"user": "exact"})
 * @ApiFilter(OrderFilter::class, properties={"id", "seesion_start"}, arguments={"orderParameterName"="order"})
 */

class Session
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read", "read:userlist"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Spots::class, inversedBy="sessions",fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read:userlist", "write"})
     */
    private $spot;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="sessions",fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read:userlist", "write"})
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read:userlist", "write"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Sport::class, inversedBy="sport", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read:userlist", "write"})
     */
    private $sport;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read:userlist", "write"})
     */
    private $seesion_start;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"read:userlist", "write"})
     */
    private $seesion_end;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSpot(): ?Spots
    {
        return $this->spot;
    }

    public function setSpot(?Spots $spot): self
    {
        $this->spot = $spot;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSport(): ?sport
    {
        return $this->sport;
    }

    public function setSport(?sport $sport): self
    {
        $this->sport = $sport;

        return $this;
    }

    public function getSeesionStart(): ?\DateTimeInterface
    {
        return $this->seesion_start;
    }

    public function setSeesionStart(\DateTimeInterface $seesion_start): self
    {
        $this->seesion_start = $seesion_start;

        return $this;
    }

    public function getSeesionEnd(): ?\DateTimeInterface
    {
        return $this->seesion_end;
    }

    public function setSeesionEnd(?\DateTimeInterface $seesion_end): self
    {
        $this->seesion_end = $seesion_end;

        return $this;
    }
}
