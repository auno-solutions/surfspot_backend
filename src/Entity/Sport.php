<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SportRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(normalizationContext={"groups"={"sport:read"}})
 * @ORM\Entity(repositoryClass=SportRepository::class)
 */
class Sport
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read:userlist","gear:read","sport:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:userlist","gear:read","sport:read"})
     */
    private $sportName;

    /**
     * @ORM\OneToMany(targetEntity=Gear::class, mappedBy="sport")
     */
    private $gears;

    /**
     * @ORM\OneToMany(targetEntity=Session::class, mappedBy="sport")
     */
    private $sport;

    /**
     * @ORM\Column(name="sport_icon",type="string", length=255, nullable=true)
     * @Groups({"read:userlist","gear:read","sport:read"})
     */
    private $sportIcon;

    /**
     * @Groups({"sport:read"})
     */
    private $sportGearType;

    /**
     * @return mixed
     */
    public function getSportGearType()
    {
        switch ($this->sportName) {
            case 'Windsurf':
                return [
                    'Board','Sail'
                ];
            case 'Surf':
                return [
                    'Board'
                ];
            case 'Kitesurf':
                return [
                    'Board','Kite'
                ];
            case 'Wing':
                return [
                    'Board','Wing'
                ];
        }

        return 'hello world';
    }

    public function __construct()
    {
        $this->gears = new ArrayCollection();
        $this->sport = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSportName()
    {
        return $this->sportName;
    }

    public function setSportName(string $sportName): self
    {
        $this->sportName = $sportName;

        return $this;
    }

    /**
     * @return Collection|Gear[]
     */
    public function getGears(): Collection
    {
        return $this->gears;
    }

    public function addGear(Gear $gear): self
    {
        if (!$this->gears->contains($gear)) {
            $this->gears[] = $gear;
            $gear->setSport($this);
        }

        return $this;
    }

    public function removeGear(Gear $gear): self
    {
        if ($this->gears->removeElement($gear)) {
            // set the owning side to null (unless already changed)
            if ($gear->getSport() === $this) {
                $gear->setSport(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Session[]
     */
    public function getSport(): Collection
    {
        return $this->sport;
    }

    public function addSport(Session $sport): self
    {
        if (!$this->sport->contains($sport)) {
            $this->sport[] = $sport;
            $sport->setSport($this);
        }

        return $this;
    }

    public function removeSport(Session $sport): self
    {
        if ($this->sport->removeElement($sport)) {
            // set the owning side to null (unless already changed)
            if ($sport->getSport() === $this) {
                $sport->setSport(null);
            }
        }

        return $this;
    }

    public function getSportIcon(): ?string
    {
        return $this->sportIcon;
    }

    public function setSportIcon(?string $sportIcon): self
    {
        $this->sportIcon = $sportIcon;

        return $this;
    }
}
