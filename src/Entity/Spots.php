<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SpotsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 *
 * @ORM\Entity(repositoryClass=SpotsRepository::class)
 */
class Spots
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read:userlist"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:userlist"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read:userlist"})
     */
    private $description;

    /**
     * @ORM\Column(type="float")
     */
    private $lattitude;

    /**
     * @ORM\Column(type="float")
     */
    private $longitude;

    /**
     * @ORM\Column(type="integer", nullable=true,options={"default" : 0})
     */
    private $likes;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="spots")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Session::class, mappedBy="spot", fetch="EAGER")
     */
    private $sessions;

    /**
     * @ORM\Column(type="boolean", nullable=false,options={"default" : 0})
     */
    private $is_public;

    /**
     * @ORM\OneToMany(targetEntity=FavoriteUserSpots::class, mappedBy="SpotId")
     */
    private $favoriteUserSpots;

    public function __construct()
    {
        $this->sessions = new ArrayCollection();
        $this->favoriteUserSpots = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLattitude(): ?float
    {
        return $this->lattitude;
    }

    public function setLattitude(float $lattitude): self
    {
        $this->lattitude = $lattitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLikes(): ?int
    {
        return $this->likes;
    }

    public function setLikes(?int $likes): self
    {
        $this->likes = $likes;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Session[]
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(Session $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->setSpot($this);
        }

        return $this;
    }

    public function removeSession(Session $session): self
    {
        if ($this->sessions->removeElement($session)) {
            // set the owning side to null (unless already changed)
            if ($session->getSpot() === $this) {
                $session->setSpot(null);
            }
        }

        return $this;
    }

    public function getIsPublic(): ?bool
    {
        return $this->is_public;
    }

    public function setIsPublic(?bool $is_public): self
    {
        $this->is_public = $is_public;

        return $this;
    }

    /**
     * @return Collection<int, FavoriteUserSpots>
     */
    public function getFavoriteUserSpots(): Collection
    {
        return $this->favoriteUserSpots;
    }

    public function addFavoriteUserSpot(FavoriteUserSpots $favoriteUserSpot): self
    {
        if (!$this->favoriteUserSpots->contains($favoriteUserSpot)) {
            $this->favoriteUserSpots[] = $favoriteUserSpot;
            $favoriteUserSpot->setSpotId($this);
        }

        return $this;
    }

    public function removeFavoriteUserSpot(FavoriteUserSpots $favoriteUserSpot): self
    {
        if ($this->favoriteUserSpots->removeElement($favoriteUserSpot)) {
            // set the owning side to null (unless already changed)
            if ($favoriteUserSpot->getSpotId() === $this) {
                $favoriteUserSpot->setSpotId(null);
            }
        }

        return $this;
    }


    public function __toString()
    {
        return (string)$this->id;

    }
}
