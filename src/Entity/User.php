<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ApiResource()
 * @ApiResource(itemOperations={
 *     "post_user"={
 *         "method"="POST",
 *         "path"="/user/creation",
 *         "controller"=AuthController::class,
 *     }
 * })
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255,unique=true)
     * @Groups({"read:userlist"})
     */
    private $username;

    /**
     * @ORM\OneToMany(targetEntity=Gear::class, mappedBy="user",fetch="EXTRA_LAZY")
     */
    private $gear;

    /**
     * @ORM\OneToMany(targetEntity=Spots::class, mappedBy="user")
     */
    private $spots;

    /**
     * @ORM\OneToMany(targetEntity=Session::class, mappedBy="user")
     */
    private $sessions;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read:userlist"})
     */
    private $public;

    /**
     * @ORM\OneToMany(targetEntity=FavoriteUserSpots::class, mappedBy="userId")
     */
    private $favoriteUserSpots;

    public function __construct()
    {
        $this->gear = new ArrayCollection();
        $this->spots = new ArrayCollection();
        $this->sessions = new ArrayCollection();
        $this->favoriteUserSpots = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier()
    {
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
//        return 'no can do';
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return Collection|Gear[]
     */
    public function getGear(): Collection
    {
        return $this->gear;
    }

    public function addGear(Gear $gear): self
    {
        if (!$this->gear->contains($gear)) {
            $this->gear[] = $gear;
            $gear->setUser($this);
        }

        return $this;
    }

    public function removeGear(Gear $gear): self
    {
        if ($this->gear->removeElement($gear)) {
            // set the owning side to null (unless already changed)
            if ($gear->getUser() === $this) {
                $gear->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Spots[]
     */
    public function getSpots(): Collection
    {
        return $this->spots;
    }

    public function addSpot(Spots $spot): self
    {
        if (!$this->spots->contains($spot)) {
            $this->spots[] = $spot;
            $spot->setUser($this);
        }

        return $this;
    }

    public function removeSpot(Spots $spot): self
    {
        if ($this->spots->removeElement($spot)) {
            // set the owning side to null (unless already changed)
            if ($spot->getUser() === $this) {
                $spot->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Session[]
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(Session $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->setUser($this);
        }

        return $this;
    }

    public function removeSession(Session $session): self
    {
        if ($this->sessions->removeElement($session)) {
            // set the owning side to null (unless already changed)
            if ($session->getUser() === $this) {
                $session->setUser(null);
            }
        }

        return $this;
    }

    public function getPublic(): ?bool
    {
        return $this->public;
    }

    public function setPublic(?bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    /**
     * @return Collection<int, FavoriteUserSpots>
     */
    public function getFavoriteUserSpots(): Collection
    {
        return $this->favoriteUserSpots;
    }

    public function addFavoriteUserSpot(FavoriteUserSpots $favoriteUserSpot): self
    {
        if (!$this->favoriteUserSpots->contains($favoriteUserSpot)) {
            $this->favoriteUserSpots[] = $favoriteUserSpot;
            $favoriteUserSpot->setUserId($this);
        }

        return $this;
    }

    public function removeFavoriteUserSpot(FavoriteUserSpots $favoriteUserSpot): self
    {
        if ($this->favoriteUserSpots->removeElement($favoriteUserSpot)) {
            // set the owning side to null (unless already changed)
            if ($favoriteUserSpot->getUserId() === $this) {
                $favoriteUserSpot->setUserId(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return (string)$this->id;
    }
}
