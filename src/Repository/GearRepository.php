<?php

namespace App\Repository;

use App\Entity\Gear;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Gear|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gear|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gear[]    findAll()
 * @method Gear[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GearRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gear::class);
    }


    public function updateUserFavoritesByGearId($data){

        $newFavoGears = $this->findOneBy(['user_default' => false, 'id' => $data['id']]);
        $sport = $newFavoGears->getSport()->getId();
        $gears = $this->findBy(['user_default' => true, 'type' => $data['type'],'sport' => $sport]);

        foreach ($gears as $gear){
            $gear->setUserDefault(false);
            $this->getEntityManager()->persist($gear);
            $this->getEntityManager()->flush();
        }

        $newFavoGears->setUserDefault(true);
        $this->getEntityManager()->persist($newFavoGears);
        $this->getEntityManager()->flush();

        return true;
    }






}
