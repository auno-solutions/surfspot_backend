<?php

namespace App\Repository;

use App\Entity\Session;
use App\Entity\Spots;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Spots|null find($id, $lockMode = null, $lockVersion = null)
 * @method Spots|null findOneBy(array $criteria, array $orderBy = null)
 * @method Spots[]    findAll()
 * @method Spots[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpotsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Spots::class);
    }

    public function generateLeaderBoard($id)
    {
        $sql = "SELECT user.username, user_id, count(*) as session_count FROM session left join user on user.id = session.user_id where user.public = 1 and spot_id = $id group by spot_id, user_id ORDER BY session_count DESC limit 5";

        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->executeQuery($sql);

        return $stmt->fetchAllAssociative();
    }

    public function findSpotDetails($id)
    {
        $sql = "SELECT spots.id, spots.user_id, spots.name, spots.description, spots.lattitude, spots.longitude, spots.likes, spots.is_public, COUNT(session.id) as session_total, COUNT(distinct session.user_id) as user_total FROM `spots` LEFT JOIN session on session.spot_id = spots.id WHERE spots.id = ";
        $sql .= $id;

        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->executeQuery($sql);

        return $stmt->fetchAllAssociative()[0];
    }

    public function findNearestSpotsByLocaiton($latitude, $longitude, $range)
    {
        $sql = "SELECT * FROM (
                    SELECT *, 
                        (
                            (
                                (
                                    acos(
                                        sin(( " . $latitude . " * pi() / 180))
                                        *
                                        sin(( spots.lattitude * pi() / 180)) + cos(( " . $latitude . " * pi() /180 ))
                                        *
                                        cos(( spots.lattitude * pi() / 180)) * cos((( " . $longitude . " - spots.longitude) * pi()/180)))
                                ) * 180/pi()
                            ) * 60 * 1.1515 * 1.609344
                        )
                    as distance FROM `spots`
                ) spots
                WHERE distance <= " . $range . "
                ORDER BY distance ASC
                LIMIT 15;
                ";

        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->executeQuery($sql);
        return $stmt->fetchAllAssociative();
    }
}
