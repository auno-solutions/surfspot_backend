<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'c2e66d2cb9948eea8c00b835a5abb96ddd4cc355',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'c2e66d2cb9948eea8c00b835a5abb96ddd4cc355',
    ),
    'api-platform/core' => 
    array (
      'pretty_version' => 'v2.6.8',
      'version' => '2.6.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ff3aab5b196709c721960c0bb4f1d52759af737d',
    ),
    'composer/package-versions-deprecated' => 
    array (
      'pretty_version' => '1.11.99.5',
      'version' => '1.11.99.5',
      'aliases' => 
      array (
      ),
      'reference' => 'b4f54f74ef3453349c24a845d22392cd31e65f1d',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.13.2',
      'version' => '1.13.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b668aef16090008790395c02c893b1ba13f7e08',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '2.1.1',
      'version' => '2.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '331b4d5dbaeab3827976273e9356b3b453c300ce',
    ),
    'doctrine/collections' => 
    array (
      'pretty_version' => '1.6.8',
      'version' => '1.6.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '1958a744696c6bb3bb0d28db2611dc11610e78af',
    ),
    'doctrine/common' => 
    array (
      'pretty_version' => '3.2.2',
      'version' => '3.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '295082d3750987065912816a9d536c2df735f637',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => '3.3.2',
      'version' => '3.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '35eae239ef515d55ebb24e9d4715cad09a4f58ed',
    ),
    'doctrine/deprecations' => 
    array (
      'pretty_version' => 'v0.5.3',
      'version' => '0.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9504165960a1f83cc1480e2be1dd0a0478561314',
    ),
    'doctrine/doctrine-bundle' => 
    array (
      'pretty_version' => '2.5.6',
      'version' => '2.5.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f1423b2a640b6ac545b6e0c02575427a4dc1e9dc',
    ),
    'doctrine/doctrine-migrations-bundle' => 
    array (
      'pretty_version' => '3.2.2',
      'version' => '3.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '3393f411ba25ade21969c33f2053220044854d01',
    ),
    'doctrine/event-manager' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41370af6a30faa9dc0368c4a6814d596e81aba7f',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '8b7ff3e4b7de6b2c84da85637b59fd2880ecaa89',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd56bf6102915de5702778fe20f2de3b2fe570b5b',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c268e882d4dbdd85e36e4ad69e02dc284f89d229',
    ),
    'doctrine/migrations' => 
    array (
      'pretty_version' => '3.4.1',
      'version' => '3.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e7df670aa9565b435ffec636cebdb4d0a1987f10',
    ),
    'doctrine/orm' => 
    array (
      'pretty_version' => '2.11.1',
      'version' => '2.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4b88ce787d3916c8366abf52f6c658a7a27ed3a6',
    ),
    'doctrine/persistence' => 
    array (
      'pretty_version' => '2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f8af155c1e7963f3d2b4415097d55757bbaa53d8',
    ),
    'doctrine/sql-formatter' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '20c39c2de286a9d3262cc8ed282a4ae60e265894',
    ),
    'fig/link-util' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5d7b8d04ed3393b4b59968ca1e906fb7186d81e8',
    ),
    'friendsofphp/proxy-manager-lts' => 
    array (
      'pretty_version' => 'v1.0.5',
      'version' => '1.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '006aa5d32f887a4db4353b13b5b5095613e0611f',
    ),
    'laminas/laminas-code' => 
    array (
      'pretty_version' => '4.5.1',
      'version' => '4.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6fd96d4d913571a2cd056a27b123fa28cb90ac4e',
    ),
    'lcobucci/clock' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '353d83fe2e6ae95745b16b3d911813df6a05bfb3',
    ),
    'lcobucci/jwt' => 
    array (
      'pretty_version' => '4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '55564265fddf810504110bd68ca311932324b0e9',
    ),
    'lexik/jwt-authentication-bundle' => 
    array (
      'pretty_version' => 'v2.14.4',
      'version' => '2.14.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b08d174fc979e12f609ad075e49b46a9e30ecfac',
    ),
    'namshi/jose' => 
    array (
      'pretty_version' => '7.2.3',
      'version' => '7.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '89a24d7eb3040e285dd5925fcad992378b82bcff',
    ),
    'nelmio/cors-bundle' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0ee5ee30b0ee08ea122d431ae6e0ddeb87f035c0',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.13.2',
      'version' => '4.13.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '210577fe3cf7badcc5814d99455df46564f3c077',
    ),
    'ocramius/package-versions' => 
    array (
      'replaced' => 
      array (
        0 => '1.11.99',
      ),
    ),
    'ocramius/proxy-manager' => 
    array (
      'replaced' => 
      array (
        0 => '^2.1',
      ),
    ),
    'paragonie/random_compat' => 
    array (
      'replaced' => 
      array (
        0 => '2.*',
      ),
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '622548b623e81ca6d78b721c5e029f4ce664f170',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.6.0',
      'version' => '1.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '93ebd0014cab80c4ea9f5e297ea48672f1b87706',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/link' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eea8e8662d5cd3ae4517c9b864493f59fca95562',
    ),
    'psr/link-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'psr/simple-cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'spatie/server-side-rendering' => 
    array (
      'pretty_version' => '0.3.2',
      'version' => '0.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '91339cc1b9d3b77d6b691d3bde4b992f9f04412a',
    ),
    'symfony/asset' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '156e45cba14f9be3fe8b987e86f482a76aa078f3',
    ),
    'symfony/cache' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b954c9397b226df319c6dfd002636ddaa4d848ef',
    ),
    'symfony/cache-contracts' => 
    array (
      'pretty_version' => 'v2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ac2e168102a2e06a2624f0379bde94cd5854ced2',
    ),
    'symfony/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/config' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd65e1bd990c740e31feb07d2b0927b8d4df9956f',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd8111acc99876953f52fe16d4c50eb60940d49ad',
    ),
    'symfony/dependency-injection' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '17f31bc13ef2b577d3c652d71af49d000cbd5894',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6f981ee24cf69ee7ce9736146d1c57c2780598a8',
    ),
    'symfony/doctrine-bridge' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '0c61df4d8311d9ff25a01d479b9c85a0df3b376d',
    ),
    'symfony/dotenv' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '83a2310904a4f5d4f42526227b5a578ac82232a9',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c4ffc2cd919950d13c8c9ce32a70c70214c3ffc5',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dec8a9f58d20df252b9cd89f1c6c1530f747685d',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '66bea3b09be61613cd3b4043a65a8ec48cfa6d2a',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/expression-language' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c68c6d1a308f6e2a1382bdb3a317959e1ee9aa08',
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '797680071ea8f71b94eb958680c50d0e002638f5',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '231313534dded84c7ecaa79d14bc5da4ccb69b7d',
    ),
    'symfony/flex' => 
    array (
      'pretty_version' => 'v1.18.5',
      'version' => '1.18.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '10e438f53a972439675dc720706f0cd5c0ed94f1',
    ),
    'symfony/form' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '18f4f8209d2db06fc3cf5ba3b2eb8e4706b34aac',
    ),
    'symfony/framework-bundle' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '59eda813401a947efefa8509322b9adee576e926',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dd68a3b24262a902bc338fc7c9a2a61b7ab2029f',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c770c90bc71f1db911e2d996c991fdafe273ac84',
    ),
    'symfony/maker-bundle' => 
    array (
      'pretty_version' => 'v1.38.0',
      'version' => '1.38.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '143024ab0e426285d3d9b7f6a3ce51e12a9d8ec5',
    ),
    'symfony/options-resolver' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cc1147cb11af1b43f503ac18f31aa3bec213aba8',
    ),
    'symfony/password-hasher' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b5ed59c4536d8386cd37bb86df2b7bd5fbbd46d4',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-iconv' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.24.0',
      'version' => '1.24.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '81b86b50cf841a64252b439e738e97f4a34e2783',
    ),
    'symfony/polyfill-intl-icu' => 
    array (
      'pretty_version' => 'v1.24.0',
      'version' => '1.24.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c023a439b8551e320cc3c8433b198e408a623af1',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.24.0',
      'version' => '1.24.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.24.0',
      'version' => '1.24.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0abb51d2f102e00a4eefcf46ba7fec406d245825',
    ),
    'symfony/polyfill-php56' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php70' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php71' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php72' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.24.0',
      'version' => '1.24.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cc5db0e22b3cb4111010e48785a97f670b350ca5',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.24.0',
      'version' => '1.24.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '57b712b08eddb97c762a8caa32c84e037892d2e9',
    ),
    'symfony/polyfill-php81' => 
    array (
      'pretty_version' => 'v1.24.0',
      'version' => '1.24.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5de4ba2d41b15f9bd0e19b2ab9674135813ec98f',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '95440409896f90a5f85db07a32b517ecec17fa4c',
    ),
    'symfony/property-access' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '95534d912f61117d3bce2d4456419ee2ee548d7a',
    ),
    'symfony/property-info' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bcc2b6904cbcf16b2e5d618da16117cd8e132f9a',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '44b29c7a94e867ccde1da604792f11a469958981',
    ),
    'symfony/security-bundle' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd6ae2f605fa8e4e0860c1a6574271af2bb4ba16c',
    ),
    'symfony/security-core' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '11d815ccbff929899a4ec545f9f85185071abd12',
    ),
    'symfony/security-csrf' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '57c1c252ca756289c2b61327e08fb10be3936956',
    ),
    'symfony/security-guard' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '3d68d9f8e162f6655eb0a0237b9f333a82a19da9',
    ),
    'symfony/security-http' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '53d572f06fc438faae3713cc97d186d941919748',
    ),
    'symfony/serializer' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e3ec068106e4cc7d17c45656bd8823788e7dc369',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1ab11b933cd6bc5464b08e81e2c5b07dec58b0fc',
    ),
    'symfony/service-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/stopwatch' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d04b5c24f3c9a1a168a131f6cbe297155bc0d30',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '92043b7d8383e48104e411bc9434b260dbeb5a10',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd28150f0f44ce854e942b671fc2620a98aae1b1e',
    ),
    'symfony/twig-bridge' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '648c8694a9470ae4aaf64cbce1b640f5941fd7c9',
    ),
    'symfony/twig-bundle' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '45ae3ee8155f93042a1033b166a7a3ed57b96a92',
    ),
    'symfony/validator' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '06c3d849fa19a0ece7b6eed240036772c4b5faaf',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v5.4.5',
      'version' => '5.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '6efddb1cf6af5270b21c48c6103e81f920c220f0',
    ),
    'symfony/var-exporter' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b199936b7365be36663532e547812d3abb10234a',
    ),
    'symfony/web-link' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '8b9b073390359549fec5f5d797f23bbe9e2997a5',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e80f87d2c9495966768310fc531b487ce64237a2',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v3.3.8',
      'version' => '3.3.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '972d8604a92b7054828b539f2febb0211dd5945c',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6964c76c7804814a842473e0c8fd15bab0f18e25',
    ),
    'willdurand/negotiation' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '68e9ea0553ef6e2ee8db5c1d98829f111e623ec2',
    ),
  ),
);
